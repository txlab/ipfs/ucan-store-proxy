use std::{fmt::Display, io::Cursor};

use anyhow::Error;
use rocket::{
    http::Status,
    outcome::try_outcome,
    request::{FromRequest, Outcome},
    response::{self, Responder},
    serde::{
        json::{self, Json},
        Deserialize, Serialize,
    },
    Request, Response,
};
use serde_json::json;

#[derive(Debug)]
pub struct AuthToken(pub String);

#[allow(dead_code)]
#[derive(Debug)]
// #[response(status = 401)]
pub enum AuthHeaderError {
    Missing,
    NotBearer(String),
    InvalidFormat(String),
    InvalidToken(String),
    Untrusted(String),
    MissingCap(String),
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for AuthToken {
    type Error = AuthHeaderError;

    async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        match request.headers().get_one("Authorization") {
            None => Outcome::Error((rocket::http::Status::Unauthorized, AuthHeaderError::Missing)),
            Some(header) => match header.split(" ").collect::<Vec<&str>>().split_first() {
                Some(split) => match split {
                    (&"Bearer", [token]) => {
                        // Got token 🎉
                        Outcome::Success(AuthToken(token.to_string()))
                    }
                    (notbear, [_token]) => Outcome::Error((
                        Status::Unauthorized,
                        AuthHeaderError::NotBearer(notbear.to_string()),
                    )),
                    _ => Outcome::Error((
                        Status::Unauthorized,
                        AuthHeaderError::InvalidFormat(header.into()),
                    )),
                },
                _ => Outcome::Error((
                    Status::Unauthorized,
                    AuthHeaderError::InvalidFormat(header.into()),
                )),
            },
        }
        // todo!("implement from_request");
    }
}

impl<'r, 'o: 'r> Responder<'r, 'o> for AuthHeaderError {
    fn respond_to(self, _: &'r Request<'_>) -> response::Result<'o> {
        let status = match &self {
            AuthHeaderError::Missing => Status::BadRequest,
            AuthHeaderError::NotBearer(_) => Status::BadRequest,
            AuthHeaderError::InvalidFormat(_) => Status::Unauthorized,
            AuthHeaderError::InvalidToken(_) => Status::Unauthorized,
            AuthHeaderError::Untrusted(_) => Status::Forbidden,
            AuthHeaderError::MissingCap(_) => Status::Forbidden,
        };

        let message = match &self {
            AuthHeaderError::Missing => "Authorization header is missing",
            AuthHeaderError::NotBearer(_) => "Authorization header is not of Bearer type",
            AuthHeaderError::InvalidFormat(s) => s,
            AuthHeaderError::InvalidToken(s) => s,
            AuthHeaderError::Untrusted(s) => s,
            AuthHeaderError::MissingCap(s) => s,
        };

        let body = json!({
            "error": message,
            "code": status.code,
        })
        .to_string();

        Response::build()
            .sized_body(body.len(), Cursor::new(body))
            .status(status)
            .ok()
    }
}

// #[catch(401)]
// pub fn catch_unauthorized(status: Status, req: &Request) -> String {
//     format!("Unauthorized: {}", status.)
// }

// struct AuthError(AuthHeaderError);

// impl<'r> Responder<'r, 'static> for AuthHeaderError {
//     fn respond_to(self, _: &'r Request<'_>) -> response::Result<'static> {
//         Error::AuthError(self).respond_to(request)
//     }
// }

// impl<'r> Responder<'r, 'static> for AuthHeaderError {
//     fn respond_to(self, _: &'r Request<'_>) -> response::Result<'static> {
//         let status = Status::Unauthorized; // or match self to return different statuses
//         let body = match self {
//             AuthHeaderError::Missing => "Authorization header is missing.",
//             AuthHeaderError::NotBearer(token) => {
//                 &format!("Expected 'Bearer' token, got '{}'", token)
//             }
//             AuthHeaderError::InvalidFormat(header) => {
//                 &format!("Invalid authorization header format: '{}'", header)
//             }
//         };

//         response::Response::build()
//             .status(status)
//             .sized_body(body.len(), std::io::Cursor::new(body))
//             .ok()
//     }
// }
