use std::env;

use anyhow::Result;
use rocket::figment::{providers::Env, Figment};
use serde::Deserialize;
use url::Url;

#[derive(Deserialize, Debug)]
pub struct AppConfig {
    // #[serde(with = "url_serde")]
    pub kubo_rpc: Url,
    #[serde(deserialize_with = "deserialize_base_url", default)]
    pub public_url: Option<Url>,
    #[serde(deserialize_with = "deserialize_base_url", default)]
    pub gateway_public_url: Option<Url>,
    #[serde(deserialize_with = "deserialize_comma_separated")]
    pub trusted_dids: Vec<String>,
}

pub fn read_config() -> Result<AppConfig> {
    let app_config = Figment::new()
        .merge(Env::raw().filter_map(|k| {
            if k == "ipfs_public_url" {
                Some("gateway_public_url".into()) // IPFS_PUBLIC_URL is an alias
            } else {
                None
            }
        }))
        .merge(Env::prefixed("UCAN_"))
        .extract()?; // TODO: integrate with rocket config
    dbg!(&app_config);
    Ok(app_config)
}

// Custom deserializer for comma-separated values
fn deserialize_comma_separated<'de, D>(deserializer: D) -> Result<Vec<String>, D::Error>
where
    D: serde::Deserializer<'de>,
{
    let s = String::deserialize(deserializer)?;
    Ok(s.split(',').map(str::trim).map(String::from).collect())
}

// Custom deserializer for domain or URL
fn deserialize_base_url<'de, D>(deserializer: D) -> Result<Option<Url>, D::Error>
where
    D: serde::Deserializer<'de>,
{
    let s: Option<String> = Option::deserialize(deserializer)?;
    match s {
        Some(s) => {
            let url = if s.starts_with("http://") || s.starts_with("https://") {
                s
            } else {
                format!("https://{}", s)
            };
            Url::parse(&url).map(Some).map_err(serde::de::Error::custom)
        }
        None => Ok(None),
    }
}
