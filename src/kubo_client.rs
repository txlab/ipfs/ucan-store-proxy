use std::io::Bytes;

use crate::{
    config::AppConfig,
    pin_structs::{AddPinRequest, Pin, PinStatus},
};
use anyhow::{bail, Context, Result};
use chrono::{DateTime, Local};
use cid::Cid;
use noosphere_ipfs::{IpfsClient, KuboClient};
use reqwest::multipart::Form;
use reqwest::multipart::Part;
use reqwest::{Body, Client};
use rocket::{data::ToByteUnit, Data};
use serde_json::{from_value, Value};

pub struct KuboState {
    pub client: KuboClient,
    pub url: url::Url,
    pub reqwest_client: Client,
}

pub fn init_kubo(config: &AppConfig) -> Result<KuboState> {
    Ok(KuboState {
        url: config.kubo_rpc.clone(),
        client: KuboClient::new(&config.kubo_rpc)?,
        reqwest_client: reqwest::Client::new(),
    })
}

pub async fn add_pin(request: &AddPinRequest, kubo: &KuboState) -> Result<PinStatus> {
    let result = kubo
        .client
        .pin_blocks(vec![&Cid::try_from(request.cid.clone())?])
        .await?;
    dbg!(&result); // ! KuboClient just discards the response - https://github.com/subconsciousnetwork/noosphere/discussions/768

    // HACK: we just return a dummy response for now
    Ok(PinStatus {
        request_id: format!("TODO-{}", request.cid),
        status: crate::pin_structs::Status::Queued,
        created: Local::now().into(),
        pin: Pin {
            cid: request.cid.to_string(),
            name: request.name.clone(),
            origins: None,
            meta: None,
        },
        delegates: vec![],
        info: None,
    })
}

pub async fn upload_car(data: Data<'_>, kubo: &KuboState) -> Result<Value> {
    let blob = data
        .open(256.megabytes()) //HACK: make configurable
        .into_bytes()
        .await?;
    if !blob.is_complete() {
        bail!("Exceeded max upload size")
    }
    let blob = blob.value;
    let form = Form::new().part("file", Part::bytes(blob));
    let response = kubo
        .reqwest_client
        .post(format!(
            "{}api/v0/dag/import?pin-roots=true", //TODO: &stats=true (requires custom json parsing bc it's returns a second JSON object on a new line)
            kubo.url
        ))
        // .body(blob)
        .multipart(form)
        .send()
        .await;
    let Ok(response) = response else {
        error!("Failed to upload: {:?}", response);
        return Err(response.context("Kubo upload").err().unwrap());
    };
    let body = response.text().await?;
    println!("{}", body);
    let json = serde_json::from_str(&body)?;
    // dbg!(&json);
    Ok(json)

    // Attempt at streaming directly to kubo:
    // let stream = data
    //     .open(256.megabytes()) //HACK: make configurable
    //     .bytes_stream()
    //     .map_ok(Bytes::from);
    // let body = Body::wrap_stream(stream);
}
