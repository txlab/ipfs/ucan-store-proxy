use noosphere_ipfs::KuboClient;
use rocket::data::{Data, ToByteUnit};
use rocket::http::Status;
use rocket::response::status;
use rocket::serde::json::Json;
use rocket::serde::{Deserialize, Serialize};
use rocket::{Route, State};
use serde_json::Value;

use crate::kubo_client::{add_pin, upload_car, KuboState};
use crate::pin_structs::{AddPinRequest, PinStatus};
use crate::ucan::capability::{StorageAction, UcanCapabilityChecks};
use crate::ucan::AuthInfo;
use crate::utils::error_response;
use crate::ApiError;

pub fn routes_upload_api() -> Vec<Route> {
    routes![route_upload]
}

#[put("/", data = "<data>")]
pub async fn route_upload(
    data: Data<'_>,
    kubo: &State<KuboState>,
    auth: AuthInfo<'_>,
) -> Result<Json<Value>, status::Custom<Json<ApiError>>> {
    auth.assert_capability(StorageAction::Store)?;

    match upload_car(data, kubo).await {
        Ok(result) => Ok(Json(result)),
        Err(err) => Err(error_response(500, format!("{:#?}", err))),
    }
}
