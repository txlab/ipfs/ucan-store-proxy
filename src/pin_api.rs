use noosphere_ipfs::KuboClient;
use rocket::http::Status;
use rocket::response::status;
use rocket::serde::json::Json;
use rocket::serde::{Deserialize, Serialize};
use rocket::{Route, State};

use crate::kubo_client::{add_pin, KuboState};
use crate::pin_structs::{AddPinRequest, PinStatus};
use crate::ucan::capability::{StorageAction, UcanCapabilityChecks};
use crate::ucan::AuthInfo;
use crate::utils::error_response;
use crate::ApiError;

pub fn routes_pin_api() -> Vec<Route> {
    routes![route_add]
}

#[post("/", data = "<data>")]
pub async fn route_add(
    data: Json<AddPinRequest>,
    kubo: &State<KuboState>,
    auth: AuthInfo<'_>,
) -> Result<Json<PinStatus>, status::Custom<Json<ApiError>>> {
    auth.assert_capability(StorageAction::Pin)?;

    match add_pin(&data, kubo).await {
        Ok(result) => Ok(Json(result)),
        Err(err) => Err(error_response(500, format!("{:#?}", err))),
    }
}
