use noosphere_ipfs::KuboClient;
use rocket::data::{Data, ToByteUnit};
use rocket::http::uri::{Origin, Segments};
use rocket::http::Status;
use rocket::response::status;
use rocket::response::Redirect;
use rocket::serde::json::Json;
use rocket::serde::{Deserialize, Serialize};
use rocket::{Request, Route, State};
use serde_json::{json, Value};
use std::path::PathBuf;

use crate::config::AppConfig;
use crate::kubo_client::{add_pin, upload_car, KuboState};
use crate::pin_structs::{AddPinRequest, PinStatus};
use crate::ucan::AuthInfo;
use crate::ApiError;

pub fn routes_gateway_redirect() -> Vec<Route> {
    routes![kubo_info, redirect_ipfs, redirect_ipns]
}

#[get("/kubo-info")]
pub async fn kubo_info(config: &State<AppConfig>) -> Result<Value, status::Custom<&'static str>> {
    let Some(ref gateway) = config.gateway_public_url else {
        return Err(status::Custom(
            Status::InternalServerError,
            "Gateway URL not configured",
        ));
    };
    Ok(json!({
        "gateway_url": gateway.to_string().trim_end_matches("/")
    }))
}

#[get("/ipfs/<_..>")] //<path..>?<params..>
pub async fn redirect_ipfs(
    config: &State<AppConfig>,
    // path: PathBuf,
    // params: PathBuf,
    // request: &'_ rocket::Request<'_>,
    uri: &Origin<'_>,
) -> Result<Redirect, status::Custom<&'static str>> {
    // dbg!(uri);
    redirect_generic(uri, config).await
}

#[get("/ipns/<_..>")] //<path..>
pub async fn redirect_ipns(
    config: &State<AppConfig>,
    uri: &Origin<'_>,
) -> Result<Redirect, status::Custom<&'static str>> {
    redirect_generic(uri, config).await
}

pub async fn redirect_generic(
    // TODO: auth: TrustedAuthInfo,
    // path: PathBuf,
    uri: &Origin<'_>,
    config: &State<AppConfig>,
) -> Result<Redirect, status::Custom<&'static str>> {
    println!("Gateway redirect: {:?}", uri);
    let Some(ref gateway) = config.gateway_public_url else {
        return Err(status::Custom(
            Status::InternalServerError,
            "Gateway URL not configured",
        ));
    };
    let new_url = format!(
        "{}{}",
        gateway.to_string().trim_end_matches("/"),
        uri.to_string()
    ); // HACK? join url and path
    Ok(Redirect::temporary(new_url))
}
