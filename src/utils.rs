use rocket::{http::Status, response::status, serde::json::Json};

use crate::ApiError;

pub fn error_response(code: u16, error: String) -> status::Custom<Json<ApiError>> {
    status::Custom(Status { code }, Json(ApiError { error }))
}
