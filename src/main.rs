#![feature(async_closure, assert_matches, decl_macro)]
#![allow(unused_imports)]

#[macro_use]
extern crate rocket;

use config::read_config;
use kubo_client::init_kubo;
use kubo_redirect::routes_gateway_redirect;
use pin_api::routes_pin_api;
use rocket::fairing::{Fairing, Info, Kind};
use rocket::http::Header;
use rocket::http::{Method, Status};
use rocket::request::FromRequest;
use rocket::response::status;
use rocket::serde::{json::Json, Serialize};
use rocket::{Request, Response};
use rocket_cors::{AllowedHeaders, AllowedOrigins};
use std::fs;
use std::net::IpAddr;
use std::process::Command;
use store_api::routes_upload_api;
use ucan::init_auth_state;

mod auth_header;
mod config;
mod kubo_client;
mod kubo_redirect;
mod pin_api;
mod pin_structs;
mod store_api;
mod ucan;
mod utils;

#[rocket::launch]
async fn rocket() -> _ {
    env_logger::init_from_env(
        env_logger::Env::default().filter_or(env_logger::DEFAULT_FILTER_ENV, "debug"),
    );

    let figment = rocket::Config::figment().merge(("address", "0.0.0.0")); // bind to all by default
    let config = read_config().expect("Read config");
    let auth_state = init_auth_state(&config).await.expect("Init auth state");
    let kubo = init_kubo(&config).expect("Init Kubo RPC client");

    let cors = rocket_cors::CorsOptions {
        allowed_origins: AllowedOrigins::all(), // ? configurable
        // allowed_origins: AllowedOrigins::some_exact(&["https://localhost:5173"]),
        allowed_methods: vec![Method::Get, Method::Post, Method::Put, Method::Options]
            .into_iter()
            .map(From::from)
            .collect(),
        allowed_headers: AllowedHeaders::some(&[
            "Authorization",
            "Accept",
            "Content-Type",
            "X-Agent-Did", //? nft.storage is using it for compute-saving spam protection
            "ucans",
        ]),
        allow_credentials: true,
        ..Default::default()
    }
    .to_cors()
    .expect("CORS setup");

    rocket::custom(figment)
        .manage(config)
        .manage(kubo)
        .manage(auth_state)
        .attach(cors)
        .mount("/health", routes![route_health]) //TODO: refactor to AdHoc stages?
        .mount("/pins", routes_pin_api())
        .mount("/upload", routes_upload_api())
        .mount("/", routes_gateway_redirect())
}

#[get("/")]
pub fn route_health() -> &'static str {
    "healthy"
}

#[derive(Serialize, Debug, PartialEq, Eq, Default)]
pub struct ApiError {
    error: String,
}
