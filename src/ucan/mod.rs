use crate::{
    auth_header::{AuthHeaderError, AuthToken},
    config::AppConfig,
    ucan::store::ShaMemoryStore,
    utils::error_response,
};
use anyhow::Result;
use anyhow::{anyhow, bail, Error};
use base64::Engine;
use did_key::{
    from_existing_key, Ed25519KeyPair, Generate, KeyMaterial as _KeyMaterial, PatchedKeyPair,
};
use ed25519_zebra::{SigningKey as Ed25519PrivateKey, VerificationKey as Ed25519PublicKey};
use rand;
use rocket::{
    http::Status,
    outcome::{try_outcome, IntoOutcome},
    request::{FromRequest, Outcome},
    response::status,
    serde::{json::Json, Deserialize, Serialize},
    tokio::sync::Mutex,
    Request,
};
use std::{cmp::Ordering, fmt::Pointer};
use ucan::{
    builder::UcanBuilder,
    capability::proof::ProofDelegationSemantics,
    chain::ProofChain,
    crypto::did::{DidParser, KeyConstructorSlice, ED25519_MAGIC_BYTES, RSA_MAGIC_BYTES},
    store::{MemoryStore, UcanJwtStore, UcanStore},
};
use ucan::{
    capability::{Ability, CapabilitySemantics, CapabilityView, Scope},
    crypto::KeyMaterial,
};
use ucan_key_support::{
    ed25519::{bytes_to_ed25519_key, Ed25519KeyMaterial},
    rsa::{bytes_to_rsa_key, RsaKeyMaterial},
};
use url::Url;
// use ucan_key_support::ed25519::Ed25519KeyMaterial;

pub mod capability;
mod store;

#[derive(Debug)]
pub struct AuthConfig {
    pub self_dids: Vec<String>,
    pub trusted_dids: Vec<String>,
    pub store: Mutex<ShaMemoryStore>, // MemoryStore is itself thread-safe, but rocket state does not give a mutable reference
}

pub async fn init_auth_state(config: &AppConfig) -> Result<AuthConfig> {
    let store = ShaMemoryStore::default(); //HACK: how do we store? (and what?)

    let self_key = generate_ed25519_key(); //TODO: store on disk
    let self_did = self_key.get_did().await?;
    let mut self_dids = vec![self_did.clone()];
    if let Some(url) = &config.public_url {
        if let Some(host) = url.host_str() {
            //TODO: verify it's not an IP (disallowed by did-web spec)
            if url.path() != "/" {
                eprintln!("WARNING: public_url has a path - not supported (yet)");
            // (i) not failing, as it's also fine to not have a web did, but still a path-based public URL
            } else {
                self_dids.push(format!(
                    "did:web:{}{}",
                    host,
                    if let Some(port) = url.port() {
                        format!("%3A{port}")
                    } else {
                        "".into()
                    }
                ))
            }
        }
    }
    println!("SELF-DIDs: {self_dids:#?}");

    let trusted_dids = config.trusted_dids.clone();
    if trusted_dids == &["*"] {
        println!("WARNING: ALL DIDS ARE TRUSTED TO AUTHORIZE STORAGE ON THIS SERVER! SHOULD ONLY BE USED FOR TESTING PURPOSES");
    }
    if trusted_dids.is_empty() {
        println!("WARNING: empty trusted_dids - generating demo ucan...");
        // let alice_key = Ed25519KeyPair::from_secret_key(&base64::engine::general_purpose::STANDARD.decode("U+bzp2GaFQHso587iSFWPSeCzbSfn/CbNHEz7ilKRZ1UQMmMS7qq4UhTzKn3X9Nj/4xgrwa+UqhMOeo4Ki8JUw==".as_bytes()).unwrap().as_slice()[0..32]);
        // let alice_keypair = from_existing_key::<Ed25519KeyPair>(
        //     &alice_key.public_key_bytes(),
        //     Some(&alice_key.private_key_bytes()),
        // );
        let demo_ucan = UcanBuilder::default()
            .issued_by(&self_key)
            .for_audience(&self_did)
            .with_lifetime(60 * 60)
            .build()?
            .sign()
            .await?
            .encode()?;
        println!("DEMO UCAN: {demo_ucan} (issuer: {self_did})");
    }
    Ok(AuthConfig {
        trusted_dids,
        self_dids,
        store: Mutex::new(store),
    })
}

#[derive(Debug)]
pub struct AuthInfo<'a> {
    pub proof: ProofChain,
    pub auth_config: &'a AuthConfig,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for AuthInfo<'r> {
    type Error = AuthHeaderError;

    async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        let token = try_outcome!(request.guard::<AuthToken>().await);
        let auth_config = try_outcome!(request
            .rocket()
            .state::<AuthConfig>()
            .or_forward(Status::InternalServerError));

        if let Some(ucans) = request.headers().get("ucans").next() {
            // https://github.com/ucan-wg/ucan-http-bearer-token?tab=readme-ov-file#41-why-disallow-duplicate-headers
            println!("Loading ucans from header to store: {ucans}");
            for token in ucans.split(',').map(|token| token.trim()) {
                match auth_config.store.lock().await.write_token(token).await {
                    Err(err) => {
                        return Outcome::Error((
                            Status::BadRequest,
                            AuthHeaderError::InvalidToken(format!("{:?}", err)),
                        ));
                    }
                    Ok(cid) => println!("Added ucan: {cid}"),
                }
            }
        }

        let validation = async || -> Result<_> {
            println!("Parsing token: {}", &token.0);
            let mut did_parser = DidParser::new(SUPPORTED_KEYS);
            let proof = ProofChain::try_from_token_string(
                &token.0,
                None, //? 'now_time' - defaults to now
                &mut did_parser,
                &*auth_config.store.lock().await,
            )
            .await?; // TODO .reduce_capabilities(semantics) ?
            println!("Proof: {:?}", &proof);
            println!("Validating token: {}", &token.0);
            proof.ucan().validate(None, &mut did_parser).await?;

            if !auth_config
                .self_dids
                .iter()
                .any(|did| did == proof.ucan().audience())
            {
                bail!(
                    "Audience is not one of my DIDs: {:?}",
                    proof.ucan().audience()
                )
            }
            Ok(proof)
        };

        match validation().await {
            Ok(proof) => Outcome::Success(AuthInfo { proof, auth_config }),
            Err(err) => {
                dbg!(&err);
                return Outcome::Error((
                    Status::Unauthorized,
                    AuthHeaderError::InvalidToken(format!("{:?}", err)),
                ));
            }
        }
    }
}

// #[derive(Debug)]
// pub struct UcanCapabilities<S, A>
// where
//         S: Scope,
//         A: Ability,
// {
//     pub capabilities: Vec<CapabilityView<S,A>>
//     pub proof: ProofChain,
// }

// #[rocket::async_trait]
// impl<'r> FromRequest<'r> for UcanCapabilities {
//     type Error = AuthHeaderError;

//     async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
//         let auth = try_outcome!(request.guard::<AuthInfo>().await);
//         let auth_state = try_outcome!(request
//             .rocket()
//             .state::<AuthConfig>()
//             .or_forward(Status::InternalServerError));
//         let trusted_dids = &auth_state.trusted_dids;
//         dbg!(&trusted_dids);

//         let validation = async || -> Result<_> {
//             if trusted_dids == &["*"] {
//                 println!("WARNING: ALL DIDS ARE TRUSTED TO AUTHORIZE STORAGE ON THIS SERVER! SHOULD ONLY BE USED FOR TESTING PURPOSES");
//                 return Ok(auth.proof);
//             }
//             println!("UCAN proof chain: {:?}", auth.proof);
//             if trusted_dids.contains(&auth.proof.ucan().issuer().into()) {
//                 return Ok(auth.proof);
//             }
//             //TODO: check UCAN proof chain
//             if auth
//                 .proof
//                 .proofs()
//                 .iter()
//                 .any(|proof| trusted_dids.contains(&proof.ucan().issuer().into()))
//             {
//                 return Ok(auth.proof);
//             }
//             bail!("UCAN issuer is not trusted: {}", auth.proof.ucan().issuer());
//         };
//         match validation().await {
//             Ok(proof) => Outcome::Success(TrustedAuthInfo {
//                 proof,
//                 self_dids: auth_state.self_dids.clone(),
//                 trusted_dids: auth_state.trusted_dids.clone(),
//             }),
//             Err(err) => {
//                 dbg!(&err);
//                 return Outcome::Error((
//                     Status::Forbidden,
//                     AuthHeaderError::Untrusted(format!("{:?}", err)),
//                 ));
//             }
//         }
//     }
// }

// #[derive(Debug)]
// pub struct TrustedUcanCapabilities {
//     pub capabilities:
//     pub proof: ProofChain,
// }

// #[rocket::async_trait]
// impl<'r> FromRequest<'r> for TrustedCapabilities {
//     type Error = AuthHeaderError;

//     async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
//         let auth = try_outcome!(request.guard::<AuthInfo>().await);
//         let auth_state = try_outcome!(request
//             .rocket()
//             .state::<AuthConfig>()
//             .or_forward(Status::InternalServerError));
//         let trusted_dids = &auth_state.trusted_dids;
//         dbg!(&trusted_dids);

//         let validation = async || -> Result<_> {
//             if trusted_dids == &["*"] {
//                 println!("WARNING: ALL DIDS ARE TRUSTED TO AUTHORIZE STORAGE ON THIS SERVER! SHOULD ONLY BE USED FOR TESTING PURPOSES");
//                 return Ok(auth.proof);
//             }
//             println!("UCAN proof chain: {:?}", auth.proof);
//             if trusted_dids.contains(&auth.proof.ucan().issuer().into()) {
//                 return Ok(auth.proof);
//             }
//             //TODO: check UCAN proof chain
//             if auth
//                 .proof
//                 .proofs()
//                 .iter()
//                 .any(|proof| trusted_dids.contains(&proof.ucan().issuer().into()))
//             {
//                 return Ok(auth.proof);
//             }
//             bail!("UCAN issuer is not trusted: {}", auth.proof.ucan().issuer());
//         };
//         match validation().await {
//             Ok(proof) => Outcome::Success(TrustedAuthInfo {
//                 proof,
//                 self_dids: auth_state.self_dids.clone(),
//                 trusted_dids: auth_state.trusted_dids.clone(),
//             }),
//             Err(err) => {
//                 dbg!(&err);
//                 return Outcome::Error((
//                     Status::Forbidden,
//                     AuthHeaderError::Untrusted(format!("{:?}", err)),
//                 ));
//             }
//         }
//     }
// }

/// FROM: https://github.com/subconsciousnetwork/noosphere/blob/main/rust/noosphere-core/src/authority/key_material.rs#L12C40-L16C3

pub const SUPPORTED_KEYS: &KeyConstructorSlice = &[
    (ED25519_MAGIC_BYTES, bytes_to_ed25519_key),
    (RSA_MAGIC_BYTES, bytes_to_rsa_key),
];

/// FROM: https://github.com/subconsciousnetwork/noosphere/blob/main/rust/noosphere-core/src/authority/key_material.rs#L19
pub fn generate_ed25519_key() -> Ed25519KeyMaterial {
    let private_key = Ed25519PrivateKey::new(rand::thread_rng());
    let public_key = Ed25519PublicKey::from(&private_key);
    Ed25519KeyMaterial(public_key, Some(private_key))
}
