use super::AuthInfo;
use crate::utils::error_response;
use crate::ApiError;
use anyhow::{anyhow, Result};
use rocket::response::status;
use rocket::serde::json::Json;
use std::cmp::Ordering;
use ucan::capability::Ability;
use ucan::capability::CapabilitySemantics;
use ucan::capability::Scope;
use url::Url;

pub trait UcanCapabilityChecks {
    fn assert_capability(
        &self,
        // resource: &str,
        capability: StorageAction,
    ) -> Result<(), status::Custom<Json<ApiError>>>;
}

pub struct StorageSemantics {}
impl CapabilitySemantics<StorageDid, StorageAction> for StorageSemantics {}

impl UcanCapabilityChecks for AuthInfo<'_> {
    fn assert_capability(
        &self,
        capability: StorageAction,
    ) -> Result<(), status::Custom<Json<ApiError>>> {
        let semantics = StorageSemantics {};
        // let raw_caps = self
        //     .proof
        //     .ucan()
        //     .capabilities()
        //     .iter()
        //     .filter_map(|cap| {
        //         // dbg!(&cap);
        //         semantics.parse_capability(&cap)
        //     })
        //     .collect::<Vec<_>>();
        // dbg!(&raw_caps);
        // for proof in self.proof.proofs() {
        //     dbg!(
        //         proof.ucan().capabilities(),
        //         proof.reduce_capabilities(&semantics)
        //     );
        // }
        let caps = self.proof.reduce_capabilities(&semantics);
        dbg!(&caps);
        let matching_caps = caps
            .iter()
            .filter(|cap_info| {
                //TODO: use 'proper' resource parsing
                // Resource == self?

                if !self
                    .auth_config
                    .self_dids
                    .iter()
                    .any(|did| cap_info.capability.resource.to_string() == *did)
                {
                    return false;
                }
                // Ability matches?
                if !(cap_info.capability.ability >= capability) {
                    return false;
                }
                return true;
            })
            .collect::<Vec<_>>();
        dbg!(&matching_caps);
        if matching_caps.iter().any(|cap_info| {
            // originator contains Owner || trusted?
            if self.auth_config.trusted_dids == &["*"] {
                println!("WARNING: ALL DIDS ARE TRUSTED TO AUTHORIZE STORAGE ON THIS SERVER! SHOULD ONLY BE USED FOR TESTING PURPOSES");
                return true
            } else  { cap_info.originators.iter().any(|originator| {
                self.auth_config.self_dids
                    .iter()
                    .any(|did| originator.to_string() == *did)
                    || self                        .auth_config.trusted_dids                        .iter()
                        .any(|did| originator.to_string() == *did)
            })
}        }) {
            Ok(())
        } else if !matching_caps.is_empty() {
            Err(error_response(
                403,
                format!("Capability not delegated from trusted originator: {matching_caps:#?}"),
            ))
        } else {
            Err(error_response(
                403,
                format!(
                    "Missing capability: '{}' on any of '{:#?}'",
                    capability.to_string(),
                    self.auth_config.self_dids
                ),
            ))
        }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct StorageDid(String);
impl TryFrom<Url> for StorageDid {
    type Error = anyhow::Error;
    fn try_from(value: Url) -> Result<Self> {
        println!("StorageDid.try_from {value}");
        match value.scheme() {
            "did" => Ok(StorageDid(String::from(value.path()))),
            _ => Err(anyhow!("Could not interpret URI as an storage: {}", value)),
        }
    }
}
impl Scope for StorageDid {
    fn contains(&self, other: &Self) -> bool {
        println!(
            "StorageDid.scope? {self:?} == {other:?} -> {}",
            self.0 == other.0
        );
        return self.0 == other.0;
    }
}
impl ToString for StorageDid {
    fn to_string(&self) -> String {
        format!("did:{}", self.0.clone())
    }
}

#[derive(PartialEq, Eq, Clone, Debug)]
pub enum StorageAction {
    Read,
    Store,
    Pin,
}
impl Ability for StorageAction {}

impl PartialOrd for StorageAction {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        println!("StorageAction.cmp {self:?}, {other:?}");
        if self == other {
            Some(Ordering::Equal)
        } else {
            None
        }
    }
}
impl TryFrom<String> for StorageAction {
    type Error = anyhow::Error;
    fn try_from(value: String) -> Result<Self> {
        println!("StorageAction.try_from {value}");
        match value.as_str() {
            "storage/read" => Ok(StorageAction::Read),
            "storage/store" => Ok(StorageAction::Store),
            "storage/pin" => Ok(StorageAction::Pin),
            _ => Err(anyhow!("Unrecognized action: {}", value)),
        }
    }
}
impl ToString for StorageAction {
    fn to_string(&self) -> String {
        match self {
            StorageAction::Read => "storage/read",
            StorageAction::Store => "storage/store",
            StorageAction::Pin => "storage/pin",
        }
        .into()
    }
}
