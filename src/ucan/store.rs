use anyhow::{anyhow, Result};
use cid::{
    multihash::{Code, MultihashDigest},
    Cid,
};
use libipld_core::{
    codec::{Codec, Decode, Encode},
    raw::RawCodec,
};
use rocket::tokio::sync::Mutex;
use std::{collections::HashMap, io::Cursor, sync::Arc};
use ucan::store::{UcanStore, UcanStoreConditionalSend};

/// A basic in-memory store that implements UcanStore for the 'raw'
/// codec. This will serve for basic use cases and tests, but it is
/// recommended that a store that persists to disk be used in most
/// practical use cases.
#[derive(Clone, Default, Debug)]
pub struct ShaMemoryStore {
    dags: Arc<Mutex<HashMap<Cid, Vec<u8>>>>,
}

#[cfg_attr(not(target_arch = "wasm32"), async_trait)]
#[cfg_attr(target_arch = "wasm32", async_trait(?Send))]
impl UcanStore<RawCodec> for ShaMemoryStore {
    async fn read<T: Decode<RawCodec>>(&self, cid: &Cid) -> Result<Option<T>> {
        let codec = RawCodec;
        let dags = self.dags.lock().await;

        Ok(match dags.get(cid) {
            Some(bytes) => Some(T::decode(codec, &mut Cursor::new(bytes))?),
            None => None,
        })
    }

    async fn write<T: Encode<RawCodec> + UcanStoreConditionalSend + core::fmt::Debug>(
        &mut self,
        token: T,
    ) -> Result<Cid> {
        let codec = RawCodec;
        let block = codec.encode(&token)?;
        let cid = Cid::new_v1(codec.into(), Code::Sha2_256.digest(&block));

        let mut dags = self.dags.lock().await;
        dags.insert(cid, block);

        Ok(cid)
    }
}
