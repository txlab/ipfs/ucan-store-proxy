use anyhow::Result;
use chrono::{DateTime, Utc};
use cid::Cid;
use rocket::serde::{
    json::{Json, Value},
    Deserialize, Serialize,
};

// Some of this was generated from openapi spec https://github.com/ipfs/pinning-services-api-spec/blob/0c8474edad00082b15f29f55e0f98d5cd067d3cb/ipfs-pinning-service.yaml
// using https://openapi-generator.tech/docs/

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, validator::Validate)]
#[serde(deny_unknown_fields)]
pub struct AddPinRequest {
    /// Content Identifier (CID) to be pinned recursively
    // pub cid: Cid, //TODO: broken parsing ("Unrecognized CID version")
    pub cid: String,

    /// Optional name for pinned data; can be used for lookups later (max 255 characters)
    #[validate(length(max = 255))]
    pub name: Option<String>,

    /// Optional list of multiaddrs known to provide the data; see Provider Hints in the docs
    #[validate(length(min = 1, max = 20))]
    pub origins: Option<Vec<String>>,

    /// Optional metadata for pin object
    pub meta: Option<Value>,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, validator::Validate)]
#[serde(deny_unknown_fields)]
pub struct PinStatus {
    /// Globally unique identifier of the pin request; can be used to check the status of ongoing pinning, or pin removal
    #[serde(rename = "requestid")]
    pub request_id: String,

    pub status: Status,

    /// Immutable timestamp indicating when a pin request entered a pinning service; can be used for filtering results and pagination
    pub created: DateTime<Utc>,

    pub pin: Pin,

    /// List of multiaddrs designated by pinning service that will receive the pin data; see Provider Hints in the docs
    // #[validate(length(min = 1, max = 20))]
    pub delegates: Vec<String>,

    /// Optional info for PinStatus response
    #[serde(skip_serializing_if = "Option::is_none")]
    pub info: Option<std::collections::HashMap<String, String>>,
}

/// Pin object
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, validator::Validate)]
#[cfg_attr(feature = "conversion", derive(frunk::LabelledGeneric))]
pub struct Pin {
    /// Content Identifier (CID) to be pinned recursively
    pub cid: String,

    /// Optional name for pinned data; can be used for lookups later
    #[validate(length(max = 255))]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,

    /// Optional list of multiaddrs known to provide the data; see Provider Hints in the docs
    #[validate(length(min = 0, max = 20))]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub origins: Option<Vec<String>>,

    /// Optional metadata for pin object
    #[serde(skip_serializing_if = "Option::is_none")]
    pub meta: Option<std::collections::HashMap<String, String>>,
}

/// Status a pin object can have at a pinning service
/// Enumeration of values.
/// Since this enum's variants do not hold data, we can easily define them as `#[repr(C)]`
/// which helps with FFI.
#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[cfg_attr(feature = "conversion", derive(frunk_enum_derive::LabelledGenericEnum))]
pub enum Status {
    #[serde(rename = "queued")]
    Queued,
    #[serde(rename = "pinning")]
    Pinning,
    #[serde(rename = "pinned")]
    Pinned,
    #[serde(rename = "failed")]
    Failed,
}
