# 'ucan store' proxy
Checks UCAN auth & forwards store/pin requests to a storage service. Part of [this endavour](https://discuss.ipfs.tech/t/all-in-one-docker-image-with-ipfs-node-best-practices/17408/)  
**Status**: in development - see [issues](https://gitlab.com/txlab/ipfs/ucan-store-proxy/-/issues) for [roadmap](https://gitlab.com/txlab/ipfs/ucan-store-proxy/-/issues/?sort=created_asc&label_name[]=feature), and [questions](https://gitlab.com/txlab/ipfs/ucan-store-proxy/-/issues/?sort=created_asc&label_name%5B%5D=question)

### Request types
- **store** CAR file / single block
- **pin** CID ([partial](https://gitlab.com/txlab/ipfs/ucan-store-proxy/-/issues/4) pinning api spec)

### Storage services
This service will proxy store requests to:
- Kubo RPC (e.g. as part of https://gitlab.com/txlab/docker/ipfs)
- [*(planned)*](https://gitlab.com/txlab/ipfs/ucan-store-proxy/-/issues/5) Generic [pinning service API](https://ipfs.github.io/pinning-services-api-spec/) (which would indirectly enable)
- [*(idea)*](https://gitlab.com/txlab/ipfs/ucan-store-proxy/-/issues/7) Filecoin?

### WIP demo
1. `cp .env.example .env`
2. run `cargo r`

(demo UCAN is in server startup log)

#### CAR upload
```
$ http -A bearer -a $ucan PUT http://localhost:8000/upload @foo.car
```
#### CID pin
```
$ http -j -A bearer -a $ucan POST http://localhost:8000/pins cid=$cid name=test-pin
```

