{ pkgs, ucan-store-proxy, inputs, ... }:

{
  image = inputs.nix2container.packages.${pkgs.system}.nix2container.buildImage {
    name = "ucan-host-ipfs";
    tag = "latest"; # ? is this good practice?
    config = {
      # Entrypoint = [ "/init" ]; #TODO: entrypoint?
      Cmd = [ (pkgs.lib.getExe ucan-store-proxy) ];
      # Cmd = [ "sh" "-c" "id && ls -al /var && touch /var/run" ];

      Env = [
        "UCAN_KUBO_RPC=http://kubo:5001"
        "UCAN_TRUSTED_DIDS=todo"
        # "UCAN_GATEWAY_PUBLIC_URL=http://localhost:8080"
      ];
      ExposedPorts = {
        "8000/tcp" = { };
      };
      #TODO: healthcheck
    };
    copyToRoot = with pkgs; [
      # Environment helpers - https://nixos.org/manual/nixpkgs/stable/#ssec-pkgs-dockerTools-helpers
      dockerTools.fakeNss # basic users & /etc/hosts setup
      dockerTools.binSh # /bin/sh - for scripts
      # dockerTools.usrBinEnv # /usr/bin/env
      dockerTools.caCertificates
    ];
    maxLayers = 15; # TODO optimize?

  };
}
