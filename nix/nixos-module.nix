{ self }:
{ config, lib, pkgs, ... }:
with lib;

let
  cfg = config.services.ucan-store-proxy;
in
{
  options.services.ucan-store-proxy = {
    enable = mkEnableOption "ucan-store-proxy";
    package = mkOption {
      type = types.package;
      default = self.ucan-store-proxy;
      description = "The package to use for ucan-store-proxy";
    };
  };

  config = mkIf cfg.enable {
    systemd.services.ucan-store-proxy = {
      description = "My Service";
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        ExecStart = "${lib.getExe cfg.package}";
        Restart = "on-failure";
      };
    };
  };
}
