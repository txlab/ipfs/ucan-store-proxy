{
  description = "Upload IPFS blocks with UCAN auth";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    devenv.url = "github:cachix/devenv";
    nix2container.url = "github:nlewo/nix2container";
    nix2container.inputs.nixpkgs.follows = "nixpkgs";
    mk-shell-bin.url = "github:rrbutani/nix-mk-shell-bin";
    rust-overlay.url = "github:oxalica/rust-overlay"; # TODO: replace with fenix?
    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    fenix = {
      # needed for devenv's languages.rust
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs@{ self, flake-parts, nixpkgs, rust-overlay, crane, fenix, devenv, ... }: (
    flake-parts.lib.mkFlake { inherit inputs; } {
      imports = [
        inputs.devenv.flakeModule
        # TODO (import rust-overlay)
      ];
      systems = [ "x86_64-linux" "i686-linux" "x86_64-darwin" "aarch64-linux" "aarch64-darwin" ];

      perSystem = { config, self', inputs', pkgs, system, ... }: (
        let

          # rustToolchain = pkgs.rust-bin.stable.latest.default.override {
          #   targets = [ "wasm32-wasi" ];
          # };
          # rustToolchain = pkgs.rust-bin.selectLatestNightlyWith (toolchain: toolchain.default.override {
          #   # extensions = [ "rust-src" ];
          #   targets = [
          #     # "x86_64-unknown-linux-musl"
          #     "wasm-unknown-unknown"
          #   ];
          # });
          rustToolchain = with fenix.packages.${system}; combine [
            minimal.rustc
            minimal.cargo
            # targets.x86_64-unknown-linux.latest.rust-std
            targets.x86_64-unknown-linux-musl.latest.rust-std
            # targets.wasm-unknown-unknown.latest.rust-std
          ];
          craneLib = (crane.mkLib pkgs).overrideToolchain rustToolchain;

          ucan-store-proxy = craneLib.buildPackage {
            # https://crane.dev/getting-started.html
            src = craneLib.cleanCargoSource (craneLib.path ./.);
            meta.mainProgram = "ucan-store-proxy";

            # CARGO_BUILD_TARGET = "wasm-unknown-unknown";
            CARGO_BUILD_TARGET = "x86_64-unknown-linux-musl";
            CARGO_BUILD_RUSTFLAGS = "-C target-feature=+crt-static";
            # Add extra inputs here or any other derivation settings
            # doCheck = true;
            buildInputs = with pkgs; [
              # openssl
              # (openssl.override { stdenv = pkgs.muslStdenv; }) # Use musl variant of OpenSSL
            ]; # see: https://discourse.nixos.org/t/rust-pkg-config-issue-on-openssl-dependency/12354/6
            nativeBuildInputs = with pkgs; [ pkg-config ];
          };
        in
        {
          # Per-system attributes can be defined here. The self' and inputs'
          # module parameters provide easy access to attributes of the same
          # system.
          checks = {
            inherit ucan-store-proxy;
          };

          packages = {
            default = ucan-store-proxy;
            docker = (import ./nix/docker.nix { inherit pkgs ucan-store-proxy inputs; }).image;
            # docker = pkgs.dockerTools.buildImage {
            #   name = "ucan-store-proxy";
            #   config = {
            #     Cmd = [ "${ucan-store-proxy}/bin/ucan-store-proxy" ];
            #   };
            #   # copyToRoot = pkgs.buildEnv {
            #   #   name = "image-root";
            #   #   paths = with pkgs; [ .. ];
            #   #   pathsToLink = [ "/bin" ];
            #   # };
            # };
          };

          devenv.shells.default = {
            # name = name;

            # https://devenv.sh/reference/options/
            packages = with pkgs; [
              nixpkgs-fmt
              nil
              # (lib.traceVal config.packages.default)
            ];

          } // (import ./devenv.nix { inherit pkgs; });
        }
      );
      flake = {
        nixosModules.default = (import ./nix/nixos-module.nix { inherit self; });
      };
    }
  );
}
